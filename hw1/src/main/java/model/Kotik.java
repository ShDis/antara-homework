/*
 * Cat life sim class
 * https://gitlab.com/ShDis/antara-homework/
 */

package model;

public class Kotik {

    private static int instancesCount = 0;

    private int prettiness;
    private String name;
    private float weight;
    private String meow;
    private float saturationLevel = 100.0f;

    //parametric constructor
    public Kotik(String name, String meow, int prettiness, float weight) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
        addInstance();
    }

    //default constructor
    public Kotik() {
        this.setKotik("Snowball", "mew", 4, 4.0f);
        addInstance();
    }

    private void setKotik(String name, String meow, int prettiness, float weight) {
        this.prettiness = prettiness;
        this.name = name;
        this.weight = weight;
        this.meow = meow;
    }

    public static int getInstancesCount() {
        return instancesCount;
    }

    private static void addInstance() {
        instancesCount++;
    }

    public int getPrettiness() {
        return prettiness;
    }

    public String getName() {
        return name;
    }

    public float getWeight() {
        return weight;
    }

    public String getMeow() {
        return meow;
    }

    public void liveAnotherDay() {
        int hour = 0;
        while (hour++ < 24) {
            String message;
            switch ((int) (Math.random() * 4 + 1)) {
                case 1:
                    if (!this.play())
                        eat();
                    break;
                case 2:
                    if (!this.sleep())
                        eat();
                    break;
                case 3:
                    if (!this.chaseMouse())
                        eat();
                    break;
                case 4:
                    if (!this.hangAround())
                        eat();
                    break;
            }
        }
    }

    private boolean play() {
        if (saturationLevel <= 0) {
            wantFoodMessage();
            return false;
        }
        System.out.println(this.name + " play");
        saturationLevel -= 25;
        return true;
    }

    private boolean sleep() {
        if (saturationLevel <= 0) {
            wantFoodMessage();
            return false;
        }
        System.out.println(this.name + " sleep");
        saturationLevel -= 10;
        return true;
    }

    private boolean chaseMouse() {
        if (saturationLevel <= 0) {
            wantFoodMessage();
            return false;
        }
        System.out.println(this.name + " chaseMouse");
        saturationLevel -= 35;
        return true;
    }

    private boolean hangAround() {
        if (saturationLevel <= 0) {
            wantFoodMessage();
            return false;
        }
        System.out.println(this.name + " hang around");
        saturationLevel -= 15;
        return true;
    }

    private void wantFoodMessage() {
        System.out.print(this.name + " is starving. ");
    }

    private void eat(float saturationLevel) {
        System.out.println(this.name + " eat");
        this.saturationLevel += saturationLevel;
    }

    private void eat(float saturationLevel, String food) {
        System.out.println(this.name + " eat " + food);
        this.saturationLevel += saturationLevel;
    }

    private void eat() {
        this.eat(100, "cookedSalmon");
    }
}
