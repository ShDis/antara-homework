/*
 * Main
 * https://gitlab.com/ShDis/antara-homework/
 */

import model.Kotik;

public class Application {
    public static void main(String[] args) {
        //cats creation
        Kotik cat1 = new Kotik("Albert", "MEOOOW", 3, 6.4f);
        Kotik cat2 = new Kotik();

        //execution of liveAnotherDay()
        System.out.println("liveAnotherDay:");
        cat1.liveAnotherDay();
        System.out.println("The End.");

        //print cat weight and name
        System.out.println("catName: " + cat1.getName() + " catWeight: " + cat1.getWeight());

        //is meow the same
        System.out.println("isMeowTheSame: " + cat1.getMeow().equals(cat2.getMeow()));

        //instances count
        System.out.println("instancesCount: " + Kotik.getInstancesCount());
    }
}
